package com.muscularcandy67;

public class ContoCorrente {

    private double conto;

    public ContoCorrente(double conto) {
        setConto(conto);
    }

    public double getConto() {
        return conto;
    }

    protected boolean setConto(double conto) {
        if(conto>=0){
            this.conto=conto;
            return true;
        }
        else return false;
    }

    public double preleva(double preleva){
        if(preleva<0){
            return -2;
        }
        if(preleva>conto){
            return -1;
        }
        return conto-=preleva;
    }

    public double accredita(double accredito){
        if(accredito<0){
            return -2;
        }
        return conto+=accredito;
    }

    public double visualizzaSaldo(){
        return getConto();
    }


}
