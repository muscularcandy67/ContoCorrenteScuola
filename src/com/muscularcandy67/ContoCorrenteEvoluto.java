package com.muscularcandy67;

import java.util.Random;

public class ContoCorrenteEvoluto {

    private String password;
    private ContoCorrente conto;

    public ContoCorrenteEvoluto(double conto) {
        this.conto = new ContoCorrente(conto);
    }

    public String makePassword(){
        char[] p = new char[6];
        Random r = new Random();
        for(int i=0; i<6; i++){
            p[i] = char.class.cast(r.nextInt());
        }
        password=p.toString();
        return password;
    }

    public boolean controlloPassword(String pw){
        return password.equals(pw);
    }

    public boolean changePassword(String pwAct, String pwNew){
        if(pwAct.equals(password)){
            if (pwNew.length()==6){
                password=pwNew;
                return true;
            }
        }
        return false;
    }


}
